import React from 'react';
import Note from './Note/Note';

const Notes = (props) => {
  return (
    props.notes.map((note, id) =>{ 
      return <Note 
        title={note.title}
        task={note.task}
        key={id}
        click={() => props.clicked(note.id)}
      />
    })
  );
};

export default Notes;