import React from 'react';
import classes from './Note.module.css';

const Note = (props) => {
  return (
    <div className={classes.Note} key={props.id}>
      <div className={classes.header}>
        <button className={classes.deleteBtn} type='button' onClick={props.click}>x</button>
      </div>
      <div className={classes.task}>
        <h4>{props.title}</h4>
        <p>{props.task}</p>
      </div>
    </div>
  );
};

export default Note;