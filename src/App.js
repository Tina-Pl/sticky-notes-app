import React, { Component } from 'react';
import classes from './App.module.css';
import Notes from './Notes/Notes';

class App extends Component {
 
  state = {
    showNote: true,
    currentNote: {
      title: '',
      task: '',
      id: ''
    },
    notes: []
  };


  onChangeHandler = (event) => {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({
      currentNote: {
        ...this.state.currentNote,
        [name]: value,
        id: `${Math.random() * 1000}`
      }
    });
  }

  addNote = () => {
    const newNote = this.state.currentNote;
    const notes = [...this.state.notes, newNote];

    this.setState({
      currentNote: {
        title: '',
        task: '',
        id: ''
    },
    notes: notes
    });
  }

  deleteNote = (id) => {
    const notes = [...this.state.notes];
    const newNotes = notes.filter(note => note.id !== id);
    this.setState({
      notes: newNotes
    })
  }

  render () {
    let notes = null;
    
    if (this.state.showNote === true) {
      notes = (
        <div className={classes.notes}>
          <Notes 
            notes={this.state.notes}
            clicked={this.deleteNote}
          />
        </div>
      )
    }

    return (
      <div className={classes.App}>
        <h1>Sticky Notes App</h1>
        <div className={classes.container}>
          <div className={classes.form}>
            <h2>Create a Note</h2>
            <input 
              type="text"
              placeholder="Enter title ..."
              name="title"
              value={this.state.currentNote.title || ''}
              onChange={this.onChangeHandler}
            />
            <input 
              type="text" 
              placeholder="Enter note ..."
              name="task"
              value={this.state.currentNote.task || ''}
              onChange={this.onChangeHandler}
            />
            
            <button className={classes.addBtn} onClick={this.addNote}>
              Add Note
            </button>
          </div>

          {notes}

        </div>
     </div>
    );
  };
};

export default App;
